<?php

function conectaDb()
{
    try {
        $tmp = new PDO('mysql:host=localhost;dbname=tienda',
         "root", "");
        #$tmp->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        #$tmp->exec("set names utf8mb4");
        //echo "salio correcto";
        
        return($tmp);
    } catch(PDOException $e) {
        echo "    <p>Error: No puede conectarse con la base de datos.</p>\n";
        echo "\n";
        echo "    <p>Error: " . $e->getMessage() . "</p>\n";
        exit();
    }
}

?>