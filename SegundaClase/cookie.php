<?php
$cookie_name = "user";
$cookie_value = "John Doe";

//metodo  nombre cookie    valor             tiempo         direccion
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- ejemplo de cookie Tech Home -->
    <!-- https://www.w3schools.com/php/php_cookies.asp -->
</head>
<body>
    
<?php
if(!isset($_COOKIE["user"])) {
    echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
    echo "Cookie '" . $cookie_name . "' is set!<br>";
    echo "Value is: " . $_COOKIE["user"];
}
?>
</body>
</html>